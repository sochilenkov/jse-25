package ru.t1.sochilenkov.tm.exception.system;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! You don't have permission for this operation...");
    }

}
