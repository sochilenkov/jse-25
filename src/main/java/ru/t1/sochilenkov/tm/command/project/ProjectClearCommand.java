package ru.t1.sochilenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Clear project list.";

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear(userId);
    }

}
